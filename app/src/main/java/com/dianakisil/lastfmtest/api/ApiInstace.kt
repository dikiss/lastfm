package com.dianakisil.lastfmtest.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiInstance {

    companion object {
        private var retrofit: Retrofit? = null
        private val BASE_URL = "http://ws.audioscrobbler.com/"
        public val keyApi = "ba81f097be02a448b50dc560182f4799"


        /**
         * Create an instance of Retrofit object
         */
        fun getRetrofitInstance(): Retrofit? {
            if (retrofit == null) {
                retrofit = retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }
    }
}