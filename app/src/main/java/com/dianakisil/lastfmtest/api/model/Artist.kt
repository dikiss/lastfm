package com.dianakisil.lastfmtest.api.model


import com.google.gson.annotations.SerializedName

class Artist(
    val name: String,
    val mbid: String,
    val url: String,
    @SerializedName("image") val images: List<Image>,
    val streamable: String
)