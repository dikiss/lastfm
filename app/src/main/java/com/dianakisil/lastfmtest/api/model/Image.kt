package com.dianakisil.lastfmtest.api.model

import com.google.gson.annotations.SerializedName

class Image(
    @SerializedName("#text") val url: String,
    val size: String
)