package com.dianakisil.lastfmtest.api

import com.dianakisil.lastfmtest.api.model.Artist
import com.google.gson.JsonElement
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/2.0/?method=artist.search&artist=cher&api_key=ba81f097be02a448b50dc560182f4799&format=json")
    fun searchArtist(@Query("artist") artist: String): Call<JsonElement>

    @GET("/2.0/?method=artist.getinfo&artist=Cher&api_key=ba81f097be02a448b50dc560182f4799&format=json")
    fun requestArtistInfo(@Query("mbid") id: String, @Query("lang") language: String): Call<Artist>

    @GET(" /2.0/?method=artist.gettopalbums&artist=cher&api_key=ba81f097be02a448b50dc560182f4799&format=json")
    fun requestAlbums(@Query("mbid") id: String, @Query("artist") artist: String): Call<JsonElement>

    @GET("/2.0/?method=album.getinfo&api_key=ba81f097be02a448b50dc560182f4799&artist=Cher&album=Believe&format=json")
    fun requestAlbum(@Query("mbid") id: String): Call<JsonElement>
}
