package com.dianakisil.lastfmtest.api.model

import com.google.gson.annotations.SerializedName

class Tracklist(
    @SerializedName("track") val tracks: List<Track>
)