package com.dianakisil.lastfmtest.api.model

class Track(
    val name: String,
    val duration: Int = 0,
    val mbid: String?,
    val url: String?,
    val artist: Artist
)