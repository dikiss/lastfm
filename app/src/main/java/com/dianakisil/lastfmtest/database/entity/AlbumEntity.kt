package com.dianakisil.lastfmtest.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.dianakisil.lastfmtest.database.converter.TracksConverters

@Entity(tableName = "album")
class AlbumEntity(val name: String,
                  @PrimaryKey
                  val mbid: String,
                  val url: String,
                  val artist: String,
                  val releaseDate: String,
                  @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
                  val image: ByteArray,
                  @TypeConverters(TracksConverters::class)
                  val tracks: List<TrackEntity>
)