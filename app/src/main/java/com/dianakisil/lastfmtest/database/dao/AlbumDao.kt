package com.dianakisil.lastfmtest.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.dianakisil.lastfmtest.database.entity.AlbumEntity


@Dao
interface AlbumDao {

    @Insert(onConflict = REPLACE)
    fun insertAlbum(albumEntity: AlbumEntity): Long

    @Delete
    fun deleteAlbum(albumEntity: AlbumEntity): Int

    @Query("select * from album")
    fun getAlbums(): LiveData<List<AlbumEntity>>


}