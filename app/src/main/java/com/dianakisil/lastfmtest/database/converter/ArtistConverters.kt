package com.dianakisil.lastfmtest.database.converter

import androidx.room.TypeConverter
import com.dianakisil.lastfmtest.database.entity.ArtistEntity
import com.google.gson.Gson

class ArtistConverters {

    var gson = Gson()

    @TypeConverter
    fun stringToTracks(data: String?): ArtistEntity {
        return gson.fromJson(data, ArtistEntity::class.java)
    }

    @TypeConverter
    fun TracksToString(someObjects: ArtistEntity): String {
        return gson.toJson(someObjects)
    }
}