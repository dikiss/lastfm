package com.dianakisil.lastfmtest.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dianakisil.lastfmtest.database.converter.ArtistConverters
import com.dianakisil.lastfmtest.database.converter.TracksConverters
import com.dianakisil.lastfmtest.database.dao.AlbumDao
import com.dianakisil.lastfmtest.database.entity.AlbumEntity
import com.dianakisil.lastfmtest.database.entity.ArtistEntity
import com.dianakisil.lastfmtest.database.entity.TrackEntity

@Database(entities = [(AlbumEntity::class), (TrackEntity::class), (ArtistEntity::class)], version = 1,exportSchema = false)
@TypeConverters(ArtistConverters::class, TracksConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun albumDao(): AlbumDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "lastFm.db")
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}