package com.dianakisil.lastfmtest.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "artist")
class ArtistEntity(
    @PrimaryKey
    val name: String,
    val mbid: String,
    val url: String,
    val image: String,
    val streamable: String? = null
)