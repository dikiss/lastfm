package com.dianakisil.lastfmtest.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.dianakisil.lastfmtest.database.converter.ArtistConverters

@Entity(tableName = "track")
class TrackEntity(
    @PrimaryKey
    val name: String,
    val duration: Int = 0,
    val mbid: String?,
    val url: String?,
    @TypeConverters(ArtistConverters::class)
    val artist: ArtistEntity
)