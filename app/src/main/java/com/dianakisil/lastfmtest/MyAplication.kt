package com.dianakisil.lastfmtest

import android.app.Application
import android.content.Context
import com.dianakisil.lastfmtest.database.AppDatabase
import com.dianakisil.lastfmtest.database.DataRepository

class MyApplication: Application() {

    companion object {
        var appContext: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        appContext = this
    }

    fun getDatabase(): AppDatabase? {
        return AppDatabase.getInstance(appContext!!)
    }

    fun getRepository(): DataRepository? {
        return DataRepository(getDatabase())
    }
}