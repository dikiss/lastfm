package com.dianakisil.lastfmtest.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.dianakisil.lastfmtest.R
import com.dianakisil.lastfmtest.ui.albums.AlbumsFragment
import com.dianakisil.lastfmtest.ui.search.ArtistFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        init()

        if(savedInstanceState == null) {
            setupFragment(AlbumsFragment::class.java)

            title = getString(R.string.text_albums)
        }

    }

    private fun init() {
        bottomNavigation.setOnNavigationItemSelectedListener(this)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val fragmentClass: Class<*>?

        when (item.itemId) {
            R.id.action_albums -> {
                fragmentClass = AlbumsFragment::class.java
            }
            R.id.action_artist -> {
                fragmentClass = ArtistFragment::class.java
            }
            else -> {
                fragmentClass = AlbumsFragment::class.java
            }
        }

        setupFragment(fragmentClass)

        item.isChecked = true
        title = item.title

        return true
    }

    private fun setupFragment(fragmentClass: Class<*>?) {
        var fragment: Fragment? = null

        try {
            fragment = (fragmentClass?.newInstance() as Fragment)
        } catch (e: Exception) {
            e.printStackTrace()
        }


        // Insert the fragment by replacing any existing fragment
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.contentFrame, fragment).commit()
        }
    }

}