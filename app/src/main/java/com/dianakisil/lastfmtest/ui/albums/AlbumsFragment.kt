package com.dianakisil.lastfmtest.ui.albums

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import androidx.recyclerview.widget.GridLayoutManager
import com.dianakisil.lastfmtest.R
import com.dianakisil.lastfmtest.database.entity.AlbumEntity
import com.dianakisil.lastfmtest.ui.BaseViewModel
import com.dianakisil.lastfmtest.ui.adapter.AlbumStoredAdapter
import com.dianakisil.lastfmtest.ui.detail.AlbumDetailActivity
import com.dianakisil.lastfmtest.ui.listener.OnClickStoredAlbumListener
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_top_albums.*

class AlbumsFragment : Fragment(), OnClickStoredAlbumListener {

    private var model: BaseViewModel? = null

    fun newInstance(): AlbumsFragment {
        return AlbumsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_albums, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = ViewModelProviders.of(this).get(BaseViewModel::class.java)

        recyclerViewAlbums.layoutManager = GridLayoutManager(activity, 2)

        if (savedInstanceState == null)
            getAlbums()
        else
            showAlbums()
    }


    @SuppressLint("FragmentLiveDataObserve")
    private fun getAlbums() {
        startProgress()
        model?.getStoredAlbums()?.observe(this, Observer {
            if (it != null && !it.isEmpty()) {
                model?.mStoredAlbumList = it

                showAlbums()

            } else {
                showEmptyView()
            }
            endProgress()
        })
    }

    private fun showAlbums() {
        val adapter = AlbumStoredAdapter(model?.mStoredAlbumList!!, this@AlbumsFragment)
        recyclerViewAlbums.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onClick(position: Int) {
        val i = Intent(activity, AlbumDetailActivity::class.java)
        i.putExtra("albumStored", model?.mStoredAlbumList!![position])
        startActivity(i)
    }


    private fun showError(desc: String) {
        Snackbar.make(rootView, desc, Snackbar.LENGTH_LONG).show()
    }

    private fun showEmptyView() {
        emptyView.visibility = View.VISIBLE
        recyclerViewAlbums.visibility = View.INVISIBLE
    }


    private fun startProgress() {
        emptyView.visibility = View.INVISIBLE
        progressBar.visibility = View.VISIBLE
    }

    private fun endProgress() {
        progressBar.visibility = View.INVISIBLE
    }
}

private fun Intent.putExtra(s: String, albumEntity: AlbumEntity) {

}
