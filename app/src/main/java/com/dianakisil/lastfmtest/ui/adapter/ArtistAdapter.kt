package com.dianakisil.lastfmtest.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dianakisil.lastfmtest.R
import com.dianakisil.lastfmtest.api.model.Artist
import com.dianakisil.lastfmtest.ui.top.TopAlbumsActivity

class ArtistAdapter(private val dataList: List<Artist>) :
    RecyclerView.Adapter<ArtistAdapter.ArtistViewHolder>() {

    var mContext: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistViewHolder {
        mContext = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.custom_view_artist, parent, false)
        return ArtistViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArtistViewHolder, position: Int) {
        holder.textNameArtist.text = dataList[position].name
        Glide.with(mContext!!).load(dataList[position].images[2].url).into(holder.imageArtist)


        holder.itemView.setOnClickListener {
            val i = Intent(mContext, TopAlbumsActivity::class.java)
            i.putExtra("artist", dataList[position])
            mContext?.startActivity(i)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ArtistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var textNameArtist: TextView
        var imageArtist: ImageView

        init {
            textNameArtist = itemView.findViewById(R.id.textArtist)
            imageArtist = itemView.findViewById(R.id.imageArtist)
        }
    }
}

private fun Intent.putExtra(s: String, artist: Artist) {

}
