package com.dianakisil.lastfmtest.ui.detail

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.dianakisil.lastfmtest.R
import com.dianakisil.lastfmtest.api.model.AlbumDetail
import com.dianakisil.lastfmtest.database.entity.AlbumEntity
import com.dianakisil.lastfmtest.toBitmap
import com.dianakisil.lastfmtest.ui.BaseViewModel
import com.dianakisil.lastfmtest.ui.adapter.TrackAdapter
import com.dianakisil.lastfmtest.ui.adapter.TrackStoredAdapter
import kotlinx.android.synthetic.main.activity_album_detail.*

class AlbumDetailActivity : AppCompatActivity() {
    private var model: BaseViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album_detail)
        init()
    }



    private fun init() {
        model = ViewModelProviders.of(this).get(BaseViewModel::class.java)

        model?.mAlbumDetail = intent.getSerializableExtra("album") as AlbumDetail?

        if (model?.mAlbumDetail != null) {

            title = model?.mAlbumDetail?.name

            textAlbum.text = model?.mAlbumDetail?.name

            Glide.with(this).load(model?.mAlbumDetail?.images!![3].url).into(imageAlbum)


            showTracks()

        } else {
            model?.mAlbumStored = intent.getSerializableExtra("albumStored") as AlbumEntity?

            title = model?.mAlbumStored?.name

            textAlbum.text = model?.mAlbumStored?.name

            imageAlbum.setImageBitmap(model?.mAlbumStored?.image?.toBitmap())


            showStoredTracks()

        }

        recyclerViewTracks.layoutManager = LinearLayoutManager(this)
    }

    fun showTracks() {
        val tracks = model?.mAlbumDetail?.tracks?.tracks
        if (tracks != null && tracks.isNotEmpty()) {
            val adapter = TrackAdapter(tracks)
            recyclerViewTracks.adapter = adapter
            adapter.notifyDataSetChanged()

            recyclerViewTracks.visibility = View.VISIBLE

            return
        }
    }

    fun showStoredTracks(){
        val tracks = model?.mAlbumStored?.tracks
        if (tracks != null && tracks.isNotEmpty()) {
            val adapter = TrackStoredAdapter(tracks)
            recyclerViewTracks.adapter = adapter
            adapter.notifyDataSetChanged()

            recyclerViewTracks.visibility = View.VISIBLE

            return
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()

        return true
    }
}