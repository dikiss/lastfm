package com.dianakisil.lastfmtest.ui.listener

interface OnClickStoredAlbumListener {
    fun onClick(position: Int)
}