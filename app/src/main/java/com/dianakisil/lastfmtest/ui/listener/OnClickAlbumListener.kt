package com.dianakisil.lastfmtest.ui.listener

interface OnClickAlbumListener {
    fun onClick(id: String)
}