package com.dianakisil.lastfmtest.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dianakisil.lastfmtest.R
import com.dianakisil.lastfmtest.api.model.Album
import com.dianakisil.lastfmtest.ui.listener.OnClickAlbumListener

class AlbumAdapter(private val dataList: List<Album>,
                   private val onClickListener: OnClickAlbumListener
) :
    RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder>() {

    var mContext: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        mContext = parent.context

        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.custom_view_top_album, parent, false)
        return AlbumViewHolder(view)
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        holder.textAlbum.text = dataList[position].name
        Glide.with(mContext!!).load(dataList[position].images[2].url).into(holder.imageAlbum)

        holder.itemView.setOnClickListener {
            if (dataList[position].mbid != null)
                onClickListener.onClick(dataList[position].mbid!!)
            else
                Toast.makeText(mContext, "Cannot get Tracks, Sorry!", Toast.LENGTH_SHORT).show()

        }

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class AlbumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var textAlbum: TextView
        var imageAlbum: ImageView

        init {
            textAlbum = itemView.findViewById(R.id.textAlbum)
            imageAlbum = itemView.findViewById(R.id.imageAlbum)
        }
    }
}